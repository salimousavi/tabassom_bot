<?php
/**
 * Created by PhpStorm.
 * User: kasper
 * Date: 3/12/17
 * Time: 2:22 PM
 */

namespace Drupal\bot\Controller;

use Drupal\comment\Entity\Comment;
use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Component\Utility\Unicode;

class BotController extends ControllerBase {

    private $chat_id;
    private $text;
    private $data;
    private $message_id;
    private $command;
    private $account;
    private $message_type;

    public function BotApi() {

        $content = file_get_contents("php://input");
        $update = json_decode($content, TRUE);


        if (isset($update['callback_query'])) {
            $this->text = $update['callback_query']['message']['text'];
            $this->data = $update['callback_query']['data'];
            $this->chat_id = $update['callback_query']['message']['chat']['id'];
            $this->message_id = $update['callback_query']['message']['message_id'];
            $message = $update['callback_query']["message"];
            $user_data = $update['callback_query']['from'];
            $this->message_type = 'callback_query';
        }
        elseif (isset($update["message"])) {

            $message = $update["message"];
            $this->text = $message['text'];
            $this->message_id = $message['message_id'];
            $this->chat_id = $message['chat']['id'];
            $user_data = $message['from'];
            $this->message_type = 'message';

        }


        $this->watchBot($this->message_id, serialize($update));

        $check = $this->UserIsExist();

        if (empty($check)) {

            if (isset($message['contact'])) {

                $contact = $message['contact'];

                if (isset($contact['user_id']) && $contact['user_id'] == $user_data['id']) {
                    $this->CreateUser($contact['phone_number']);
                }
            }

            $this->GetMobile();
        }
        else {

            $account = User::load(reset($check));

            if ($account->get('field_nickname')->isEmpty()) {

            }

            $this->account = $account;

            if ($this->message_type == 'callback_query') {

                if ($this->data == CANCEL) {
                    $this->MainMessage(true);
                }

                $command = (array)$this->GetCommand();

                if (isset($command['command'])) {

                    $this->command = $command['command'];

                    if ($this->CheckCommand('GET_RELATION')) {

                        $this->SetUserRelation();
                        $this->ClearCommand();
                        $this->CreateCommand('GET_PERSONNEL_CODE');
                        $message = "کد پرسنلی خویشاوند خود را وارد نمایید.";
                        $this->SendMessage($message);


                    }

                    if ($this->CheckCommand('GET_USER_TYPE')) {


                        $this->SetUserType();
                        $this->ClearCommand();

                        switch ($this->data) {
                            case '1':
                                $this->CreateCommand('GET_PERSONNEL_CODE');
                                $message = "کد پرسنلی خود را وارد نمایید.";
                                $this->SendMessage($message);
                            case '2':
                                $this->CreateCommand('GET_RELATION');
                                $message = "نسبت خود با همکار انتخاب را انتخاب نمایید.";
                                $this->EditMessage($message, convert_to_callback($this->GetUserRelation()));
                        }
                    }
                }
            }

            if ($this->message_type == 'message') {
                if ($this->CheckText(CANCEL)) {
                    $this->MainMessage(true);
                }

                if ($this->CheckText(IGNORE)) {
                    $this->SendPuzzle();
                }

                if ($this->CheckText(EDIT_PROFILE)) {
                    $this->CreateCommand('ENTER_NICKNAME');
                    $this->SendMessage("نام و نام خانوادگی خود را وارد نمایید.");
                }

                if ($this->CheckText(GET_PUZZLE)) {
                    $this->SendPuzzle();
                }


                $command = (array)$this->GetCommand();

                if (isset($command['command'])) {

                    $this->command = $command['command'];

                    if ($this->CheckCommand('ENTER_NICKNAME')) {

                        $account->set('field_nickname', $this->text);
                        $account->save();
                        $this->ClearCommand();
                        $this->CreateCommand('GET_USER_TYPE');
                        $this->SendMessage('نوع ارتباط خود با گروه انتخاب را انتخاب کنید.', convert_to_callback($this->GetUserTypes()));

                    }

                    if ($this->CheckCommand('GET_PERSONNEL_CODE')) {

                        $this->SetPersonnelCode();
                        $this->ClearCommand();
                        $this->CreateCommand('GET_REAGENT');
                        $message = "درصورتی که از سمت یکی از همکاران معرفی شده اید کد پرسنلی او را وارد نمایید در غیر این صورت بر روی ادامه کلیک کنید.";
                        $this->SendMessage($message, [IGNORE]);
                    }

                    if ($this->CheckCommand('GET_REAGENT')) {
                        $this->SetUserReagent();
                        $this->ClearCommand();
                        $this->MainMessage();
                    }

                    if ($this->CheckCommand('GET_PUZZLE')) {
                        $this->CreatePuzzleSolution();
                        $this->ClearCommand();
                        $message = "پاسخ شما ثبت شد. با تشکر از همراهی شما.";
                        $this->SendMessage($message, main_menu());
                    }
                }

            }


            if ($account->get('field_nickname')->isEmpty()) {
                $this->CreateCommand('ENTER_NICKNAME');
                $this->SendMessage("نام و نام خانوادگی خود را وارد نمایید.");
            }

            $this->MainMessage();

        }

        return new JsonResponse(array(TRUE));
    }


    public function TestApi() {

        $this->SendPuzzle();

        $nids = \Drupal::entityQuery('node')
            ->condition('type','puzzle')
            ->sort('nid', 'DESC')
            ->range(0,1)
            ->execute();

        $node = Node::load(reset($nids));

        $r = file_create_url($node->field_image->entity->uri->value);

            var_dump($r);die();
        $desc = $node->get('body')->getValue();
        var_dump($desc[0]['value']);die();
        //        var_dump(random_int(0, 20000));die();
        $items = [
            '83394005',
            '162090274',
            '67829038',
        ];
        foreach ($items as $item) {
            $response = apiRequest("sendMessage", [
                'chat_id' => $item,
                'text'    => random_int(0, 20000) . 'Hello World',
            ]);
            sleep(40);
            var_dump($response);
        }

        return new JsonResponse('salam');
        //        var_dump(
        //            apiRequestJson("sendMessage", array(
        //            'chat_id' => '83394005',
        //            'text'    => 'salam شناشره',
        //        )));
        //        sleep(90);
        //        var_dump(apiRequestJson("sendMessage", array(
        //            'chat_id' => '162090274',
        //            'text'    => 'okookoko',
        //        )));
        //        die();

        $w = $this->GetWatch();
        var_dump(unserialize($w->message));
        die();

        return new JsonResponse(array(TRUE));
    }

    public function GetMobile() {
        apiRequestWebhook("sendMessage", array(
            'chat_id'      => $this->chat_id,
            "text"         => 'برای شروع ابتدا باید اطلاعات خود را ثبت کرده و سپس میتوانید از امکانات این ربات استفاده نمایید
            برای ادامه، با فشردن دکمه زیر شماره همراه خود را تایید کنید.',
            'reply_markup' => array(
                'keyboard'          => array(
                    array(
                        array(
                            'text'            => "تایید شماره همراه",
                            'request_contact' => TRUE,
                        ),
                    ),
                ),
                'one_time_keyboard' => TRUE,
                'resize_keyboard'   => TRUE,
                'ForceReply'        => TRUE,
            ),
        ));
    }

    public function CheckText($text) {

        if ($this->text === $text) {
            return true;
        }

        return false;
    }

    public function CheckCommand($command) {

        if ($this->command === $command) {
            return true;
        }

        return false;
    }

    public function MainMessage($clear = false) {

        if ($clear) {
            $this->ClearCommand();
        }

        $message = "از منو زیر استفاده نمایید.";
        $this->SendMessage($message, main_menu());

        return true;
    }

    public function SendMessage($message, $options = [], $cancel = false) {

        $request = [
            'chat_id' => $this->chat_id,
            'text'    => $message,
        ];

        if (isset($options[0]['text'])) {

            if ($cancel) {
                $options[] = [
                    'text'          => CANCEL,
                    'callback_data' => CANCEL,
                ];
            }

            $request['reply_markup'] = [
                'inline_keyboard' => array_chunk($options, 1),
                'remove_keyboard' => true,
            ];

        }
        elseif (!empty($options)) {
            if ($cancel) {
                $options[] = CANCEL;
            }

            $request['reply_markup'] = [
                'keyboard'          => array_chunk($options, 2),
                'one_time_keyboard' => TRUE,
                'resize_keyboard'   => TRUE,
            ];
        }
        else {
            $request['reply_markup']['remove_keyboard'] = true;
        }

        apiRequestWebhook("sendMessage", $request);
    }

    public function EditMessage($message, $options = [], $cancel = false) {

        $request = [
            'chat_id'    => $this->chat_id,
            'text'       => $message,
            'message_id' => $this->message_id,
        ];

        if (!isset($options[0]['text'])) {

            if ($cancel) {
                $options[] = CANCEL;
            }

            $request['reply_markup'] = [
                'keyboard'          => array_chunk($options, 2),
                'one_time_keyboard' => TRUE,
                'resize_keyboard'   => TRUE,
            ];
        }
        else {

            if ($cancel) {
                $options[] = [
                    'text'          => CANCEL,
                    'callback_data' => CANCEL,
                ];
            }

            $request['reply_markup'] = [
                'inline_keyboard' => array_chunk($options, 1),
            ];
        }

        apiRequestWebhook("editMessageText", $request);
    }

    public function GetWatch() {
        $query = \Drupal::database()->select('watchbot', 'c');
        $query->fields('c');
        $query->orderBy('c.id', 'DESC');
        $query->range(0, 1);

        return $query->execute()->fetchObject();
    }


    public function UserIsExist() {
        $query = \Drupal::entityQuery('user');
        $result = $query->Condition('field_telegram_id.value', $this->chat_id, '=')->range(0, 1)->execute();

        return $result;
    }

    public function CreateUser($phone_number) {

        $phone_number = Unicode::substr($phone_number, -10);

        $account = user_load_by_mobile($phone_number);

        if ($account) {
            $account->set('field_telegram_id', $this->chat_id);
            $account->save();
        }
        else {

            $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
            $user = \Drupal\user\Entity\User::create();

            //Mandatory settings
            $user->setPassword(user_password(8));
            $user->enforceIsNew();
            //    $user->setEmail('test@test.com');
            $user->setUsername($phone_number); //This username must be unique and accept only a-Z,0-9, - _ @ .

            //Optional settings
            $user->set("init", 'email');
            $user->set("langcode", $language);
            $user->set("preferred_langcode", $language);
            $user->set("preferred_admin_langcode", $language);
            //$user->set("setting_name", 'setting_value');
            $user->activate();

            $user->set('field_telegram_id', $this->chat_id);
            $user->set('field_mobile', $phone_number);

            //Save user
            $user->save();
        }

        $message = "اطلاعات شما به درستی ذخیره شد.منتظر تایید مدیر باشید.";
        $this->SendMessage($this->chat_id, $message, ['بررسی وضعیت']);

        return true;
    }

    public function CreateCommand($command) {
        $conn = \Drupal::database();
        $conn->insert('bot_command')->fields(array(
            'command' => $command,
            'user_id' => $this->chat_id,
        ))->execute();

        return TRUE;
    }


    public function watchBot($id, $message) {
        $conn = \Drupal::database();
        $conn->merge('watchbot')->keys(array(
            'id' => $id,
        ))->fields(array(
            'id'      => $id,
            'message' => $message,
        ))->execute();

        return TRUE;
    }


    public function UpdateCommand($command_id, $tid = null) {
        $query = \Drupal::database();
        $query->update('bot_command')->fields(array(
            'answer' => $this->text,
            'tid'    => $tid,
        ))->condition('id', $command_id)->execute();

        return TRUE;
    }

    public function GetCommand() {
        $query = \Drupal::database()->select('bot_command', 'c');
        $query->fields('c');
        $query->condition('c.user_id', $this->chat_id);
        $query->orderBy('c.id', 'DESC');
        $query->range(0, 1);

        return $query->execute()->fetchObject();
    }

    public function GetPreviousCommand() {
        $query = \Drupal::database()->select('bot_command', 'c');
        $query->fields('c');
        //        $query->condition('c.user_id', $this->chat_id);
        $query->orderBy('c.id', 'DESC');
        $query->range(0, 2);

        return end($query->execute()->fetchAll());
    }

    public function SendPuzzle() {

        $this->CreateCommand('GET_PUZZLE');

        $nids = \Drupal::entityQuery('node')
            ->condition('type','puzzle')
            ->sort('nid', 'DESC')
            ->range(0,1)
            ->execute();

        $node = Node::load(reset($nids));


        $desc= $node->get('body')->getValue();

        $request = [
            'chat_id' => $this->chat_id,
            'photo'    => file_create_url($node->field_image->entity->uri->value),
            'caption' => $desc[0]['value'],
        ];

//        var_dump($request);die();

//        $this->SendMessage('sdd', convert_to_callback($this->GetUserTypes()));

//        apiRequestWebhook("sendMessage", $request);
        apiRequestWebhook("sendPhoto", $request);

    }

    public function SetUserType() {
        $this->account->set('field_user_type', [$this->data]);
        $this->account->save();
    }

    public function SetUserRelation() {
        $this->account->set('field_user_relation', [$this->data]);
        $this->account->save();
    }

    public function SetUserReagent() {
        $this->account->set('field_reagent', $this->text);
        $this->account->save();
    }

    public function SetPersonnelCode() {
        $this->account->set('field_personnel_code', $this->text);
        $this->account->save();
    }

    public function CreatePuzzleSolution() {


        $nids = \Drupal::entityQuery('node')
            ->condition('type','puzzle')
            ->sort('nid', 'DESC')
            ->range(0,1)
            ->execute();
        $nid = reset($nids);

        $values = [

            // These values are for the entity that you're creating the comment for, not the comment itself.
            'entity_type' => 'node',            // required.
            'entity_id'   => $nid,                // required.
            'field_name'  => 'comment',         // required.

            // The user id of the comment's 'author'. Use 0 for the anonymous user.
            'uid' => $this->account->id(),                         // required.

            // These values are for the comment itself.
            'comment_type' => 'comment',        // required.
            'subject' => $this->account->get('field_nickname'),  // required.
            'comment_body' => $this->text,            // optional.

            // Whether the comment is 'approved' or not.
            'status' => 1,                      // optional. Defaults to 0.
        ];

        // This will create an actual comment entity out of our field values.
        $comment = Comment::create($values);

        // Last, we actually need to save the comment to the database.
        $comment->save();;
    }

    public function ClearCommand() {
        $query = \Drupal::database()->delete('bot_command');
        $query->condition('user_id', $this->chat_id)->execute();

        return TRUE;
    }

    public function GetUserTypes() {
        $terms = \Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadTree('user_type');

        $result = array();

        foreach ($terms as $term) {
            $result[$term->tid] = $term->name;
        }

        return $result;
    }

    public function GetUserRelation() {
        $terms = \Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadTree('user_relation');

        $result = array();

        foreach ($terms as $term) {
            $result[$term->tid] = $term->name;
        }

        return $result;
    }

}